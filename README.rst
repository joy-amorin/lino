====================
The ``lino`` package
====================





This is the core package of the Lino framework.

This repository is an integral part of the `Lino framework
<https://www.lino-framework.org>`__, which is documented as a whole in the `Lino
Book <https://dev.lino-framework.org/about/overview.html>`__.

- Code repository: https://gitlab.com/lino-framework/lino
- Test results: https://gitlab.com/lino-framework/lino/-/pipelines
- Feedback: https://community.lino-framework.org
- Maintainer: https://www.saffre-rumma.net/


