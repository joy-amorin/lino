��    i      d  �   �       	     	     		  	   #	     -	     ;	     ?	     E	  	   S	     ]	     k	  
   r	     }	  	   �	     �	     �	     �	     �	     �	     �	     �	     �	     �	     �	  
   
     
     
     
     +
  	   4
  	   >
     H
     M
     V
     Z
     f
     l
     s
     x
     |
     �
     �
     �
  	   �
     �
     �
     �
     �
     �
     �
               %     +     3     @     M     U     \     d     l     �     �     �     �  
   �     �     �     �     �     �  1   �          +     E  Q   U     �     �     �     �     �  
   �  	   �  
   �     �       +   	  .   5     d     �  	   �  6   �  	   �  I   �               6     ;     A     Z     a     f     o     w       `  �     �     �          /     >     D     L     ]     i     z     �     �     �     �     �     �     �  	   �     �     �     �     �     �                         '     ,     3     <     A     J     M     Y     a     j     o     s     z     }     �     �     �     �     �     �  &   �     �  	   �     �       	     
     
   (     3     :  	   B     L     U     s     |     �     �     �     �     �  
   �     �     �  #   �     �  &        E  M   U     �     �     �     �     �     �     �     �            +   #  *   O      z     �     �  1   �     �  @   �     &     -     0     5     B     E  	   L     V     c     p     y     >   f             (       7                	   ?   -   M   b   )                       @       g       _       P   *   #       K      N   &       U   T       I   '   J   "   Q   6       !       O   S   ,   X          %                :               [              +   V   0       d   .          L       A       9   i   \       1   3       
          B   H   F   E                    e       D       c       ^      Y               =   4               C   G             R                  ;          Z   `   h   $          <   a   2       /   5      W   8   ]        using  %(details)s of %(master)s Act as... Administrator Age Alert All {0} files Anonymous Are you sure? Author Birth date Change password Configure Confirmation Create Created Delete Description Designation Displaying {0} - {1} of {2} Edit End date Female First name Gender General Hi, %(user)s! Language Languages Last name Male Modified Mrs My settings My {} Myself Name New New {} No No data to display No. Nonbinary Note Notification message Notification messages Office Or sign in using social auth: Or {} as a new user. Page Please wait... Print Printed Quick Search Quick links: Refresh Remark Remarks Reports Row %(rowid)d of %(rowcount)d Save See as  Select a %s... Sign out Start date State Subject Summary System Take The password is the same for all of them: "1234". This demo site has %d users: This is a Lino demo site. Total (%d rows) Try also the other <a href="http://lino-framework.org/demos.html">demo sites</a>. Upload Upload file Upload files Uploaded by User User roles User type User types Username Warning We are running %(name)s version %(version)s We are running with simulated date set to {0}. Welcome to the <b>%s</b> site. Yes Yes or no You are about to delete %(num)d %(type)s
(%(targets)s) You have  Your feedback is welcome to %s or directly to the person who invited you. at indirect salutationMr name never nominative salutationMr of {0} once register sign in unknown {0} items in {1} Project-Id-Version: 
Report-Msgid-Bugs-To: 
PO-Revision-Date: 2023-11-24 05:26+0200
Last-Translator: Luc Saffre <luc.saffre@gmail.com>
Language-Team: et
Language: et
MIME-Version: 1.0
Content-Type: text/plain; charset=utf-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=2; plural=(n != 1);
Generated-By: Babel 2.12.1
X-Generator: Poedit 3.0.1
  kasutades  %(master)s'i %(details)s Tööta teise kasutajana Administraator Vanus Hoiatus Kõik {0} failid Anonüümne Kas oled kindel? Autor Sünnipäev Muuda salasõna Seaded Kinnitus Loo Loodud Kustuta Kirjeldus Nimetus Read  {0} - {1}  ({2}st) Muuda Lõpeb (päev) Naine Eesnimi Sugu Üld Tere, %(user)s! Keel Keeled Perenimi Mees Muudatud Pr Minu seaded Minu {} Mina ise Nimi Uus Uus {} Ei (tühi) Nr. Mittebinaarne Märkus Teade Teaded Büroo Või sisselogida sotsiaalmeedia kaudu: Või {} uueks kasutajaks. Lehekülg Oota palun... Trüki Trükitud Kiirotsing Kiirviited Uuenda Märkus Märkused Aruanded Rida %(rowid)d / %(rowcount)d Salvesta Näita kui  Vali mõni %s... Logi välja Algab (päev) Staatus Teema Kokkuvõte Süsteem Võta Parooliks on neil kõikidel "1234". Sellel saidil on %d kasutajat: Tere tulemast selle Lino demo saidile. Kokku (%d rida) Vaata ka teisi <a href="http://lino-framework.org/demos.html">demo saite</a>. Laadi ülesse Laetud fail Laetud failid Ülesselaadija Kasutaja Kasutajarollid Kasutajaliik Kasutajaliigid Kasutajanimi Hoiatus Siin jookseb %(name)s (version %(version)s) Me töötame simuleeritud kuupäevaga {0}. Tere tulemast <b>%s</b> saidile. Jah Jah või ei Sa tahad kustutada %(num)d %(type)s
(%(targets)s) Sul on  Kui vajad abi, siis küsi aadressilt %s või otse autori käest.  kell  Hr nimi mitte kunagi Hr  / {0} üks kord registreerda sisse logida tundmatu {0} rida tabellis {1} 